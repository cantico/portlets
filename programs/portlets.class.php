<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
bab_functionality::get('Widgets')->includePhpClass('Widget_Item');
require_once dirname(__FILE__).'/functions.php';


/**
 * The Portlets functionality gives access the configuration of
 * portlet containers.
 */
class Func_Portlets extends bab_functionality
{
    public function getDescription()
    {
    	return portlets_translate('Portlets');
    }


    /**
     *
     * @return portlets_ContainerConfigurationSet
     */
    public function containerConfigurationSet()
    {
        require_once dirname(__FILE__).'/containerconfiguration.class.php';
        $containerConfigurationSet = new portlets_ContainerConfigurationSet();

        return $containerConfigurationSet;
    }


    /**
     * Returns the container configuration for the specified container.
     * If $create is true, creates a new configuration for the container if it does not already exist.
     *
     * @param string    $container  The container identifier.
     * @param boolean   $create     True to create a new configuration for the container if it does not already exist.
     * @return portlets_ContainerConfiguration
     */
    public function getContainerConfiguration($container, $create = true)
    {
        $containerConfigurationSet = $this->containerConfigurationSet();

        $containerConfiguration = $containerConfigurationSet->get($containerConfigurationSet->container->is($container));
        if (!$containerConfiguration) {
            $containerConfiguration = $containerConfigurationSet->newRecord();
            $containerConfiguration->container = $container;
            $containerConfiguration->save();
        }
        return $containerConfiguration;
    }


    /**
     * @since 0.22.0
     * @return portlets_PortletConfigurationSet
     */
    public function portletConfigurationSet()
    {
        require_once dirname(__FILE__).'/portletconfiguration.class.php';
        $portletConfigurationSet = new portlets_PortletConfigurationSet();
        return $portletConfigurationSet;
    }



    /**
     * Returns the portlet configuration for the specified portlet.
     *
     * @since 0.22.0
     *
     * @param string    $portlet  The portlet identifier.
     *
     * @return portlets_PortletConfiguration
     */
    public function getPortletConfiguration($portlet)
    {
        $portletConfigurationSet = $this->portletConfigurationSet();

        $portletConfiguration = $portletConfigurationSet->get($portletConfigurationSet->id->is($portlet));

        return $portletConfiguration;
    }
}
