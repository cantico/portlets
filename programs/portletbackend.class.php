<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
bab_Widgets()->includePhpClass('Widget_Item');


/**
 * Portlet backend
 */
class Func_PortletBackend extends bab_functionality
{
    /**
     * {@inheritDoc}
     * @see bab_functionality::getDescription()
     */
    public function getDescription()
    {
        return portlets_translate('Portlet backend');
    }


    /**
     * Returns an array of PortletDefinitions corresponding to the specified category
     * or all PortletDefinitions if $category is not specified.
     *
     * @param string $category		internationalized string visible in search engine
     *
     * @return	portlet_PortletDefinitionInterface[]
     */
    public function select($category = null)
    {
        return array();
    }


    /**
     * get a list of categories supported by the backend
     * @return Array
     */
    public function getCategories()
    {
        // Example from netvibes :

        return array(
            'featured' 	=> portlets_translate('Featured'),
            'news'		=> portlets_translate('News'),
            'business'	=> portlets_translate('Business'),
            'sport'		=> portlets_translate('Sport'),
            'media'		=> portlets_translate('TV, movies and music'),
            'tools'		=> portlets_translate('Tools and technology'),
            'fun'		=> portlets_translate('Fun & games'),
            'lifestyle'	=> portlets_translate('Lifestyle'),
            'shopping'	=> portlets_translate('Shopping'),
            'travel'	=> portlets_translate('Travel')
        );
    }



    /**
     * Get portlet definition instance
     * @param	string	$portletId			portlet definition ID
     *
     * @return portlet_PortletDefinitionInterface
     */
    public function getPortletDefinition($portletId)
    {
        $portletConstructorMethod = 'portlet_' . $portletId;
        if (method_exists($this, $portletConstructorMethod)) {
            return $this->$portletConstructorMethod();
        }

        return null;
    }



    /**
     * Create an instance of displayable portlet from the portlet definition
     * @param	string	$portletId			portlet definition ID
     *
     * @return portlet_PortletInterface
     */
    public function getPortlet($portletId)
    {
        $definition = $this->getPortletDefinition($portletId);

        if (!$definition) {
            // the definition has been deleted from the target backend
            return null;
        }

        return $definition->getPortlet();
    }


    /**
     * Return a list of actions to use for configuration.
     * When fired, a parameter named backurl will be added to action, this parameter will contain an url to get back to the main configuration page.
     * Each action can contain an icon and a title.
     *
     * @return Widget_Action[]
     */
    public function getConfigurationActions()
    {
        return array();
    }


    /**
     * Can be call in an init addon file.
     * It add a specific portlet to a specific container position
     *
     * @param string            $backend
     * @param string            $portlet
     * @param string            $containerId
     * @param int               $position
     * @param string|array      $preferences	empty => no configuration
     * @return boolean
     */
    public static function addPortlet($backend, $portlet, $containerId, $position, $preferences = '')
    {
        require_once dirname(__FILE__).'/portletconfiguration.class.php';
        $set = new portlets_PortletConfigurationSet();
        
        if(is_array($preferences)){
            $preferences = serialize($preferences);
        }

        $new = $set->newRecord();
        $new->backend = $backend;
        $new->portletDefinition = $portlet;
        $new->container = $containerId;
        $new->position = $position;
        $new->preferences = $preferences;
        $new->save();

        return true;
    }


    /**
     * Sets a single preference for the specified portlet.
     *
     * @param int       $portletId      The portlet id
     * @param string    $name           The preference name
     * @param mixed     $value          The preference value
     *
     * @return bool
     */
    public static function setPreference($portletId, $name, $value)
    {
        require_once dirname(__FILE__).'/portletconfiguration.class.php';
        $portletConfigurationSet = new portlets_PortletConfigurationSet();
        $configuration = $portletConfigurationSet->get($portletId);

        if (!isset($configuration)) {
            return false;
        }
        if (!$configuration->canUpdatePreferences()) {
            return false;
        }

        $preferences = $configuration->getPreferencesValues();

        $preferences[$name] = $value;

        $configuration->preferences = serialize($preferences);
        return $configuration->save();
    }

    /**
     * Check is a specific portlet container is empty
     *
     * @param string 	$containerId
     * @return boolean
     */
    public static function isEmptyContainer($containerId)
    {
        require_once dirname(__FILE__).'/portletconfiguration.class.php';
        $set = new portlets_PortletConfigurationSet();

        $container = $set->get($set->container->is($containerId));
        if($container){
            return false;
        }

        return true;
    }
}



interface portlet_PortletDefinitionInterface
{
    /**
     * @return string
     */
    public function getId();


    /**
     * Internationalized name of portlet
     * @return string
     */
    public function getName();


    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return portlet_PortletInterface
     */
    public function getPortlet();

    /**
     * @return array
     */
    public function getPreferenceFields();

    /**
     * Returns the widget rich icon URL.
     * 128x128 ?
     *
     * @return string
     */
    public function getRichIcon();


    /**
     * Returns the widget icon URL.
     * 48x48 px
     *
     * @return string
     */
    public function getIcon();


    /**
     * Get thumbnail URL
     * max 120x60
     */
    public function getThumbnail();



    /**
     * Return a list of actions to use for configuration.
     * When fired, a parameter named backurl will be added to action, this parameter will contain an url to get back to the main configuration page.
     * Each action can contain an icon and a title.
     *
     * @return Widget_Action[]
     */
    public function getConfigurationActions();
}





abstract class portlet_PortletDefinition implements portlet_PortletDefinitionInterface
{
    /**
     * (non-PHPdoc)
     * @see portlet_PortletDefinitionInterface::getIcon()
     */
    public function getIcon()
    {
        $id = $this->getId();

        $addon = bab_getAddonInfosInstance('portlets');
        $uploadPath = $addon->getUploadPath();
        $path = new bab_Path($uploadPath, $id);
        $path->createDir();
        $fileUrl = false;
        foreach ($path as $file) {
            if ($file->isFile()) {
                $fileUrl = $file->tostring();
                $T = @bab_functionality::get('Thumbnailer');

                if ($T) {
                    // The thumbnailer functionality is available.
                    $T->setSourceFile($fileUrl);
                    $fileUrl = $T->getThumbnail(48, 48);
                }
                break;
            }
        }

        if (!$fileUrl) {
            $fileUrl = $this->getIcon();
            if (!$fileUrl) {
                $fileUrl = $addon->getStylePath().'preferences_plugin.png';
            }
        }

        return $fileUrl;
    }


    public function setIcon(bab_Path $iconPath)
    {
        $id = $this->getId();

        $addon = bab_getAddonInfosInstance('portlets');
        $portletUploadPath = new bab_Path($addon->getUploadPath(), $id);

        try {
            $portletUploadPath->deleteDir();
        } catch (bab_FolderAccessRightsException $e) { }
        $portletUploadPath->createDir();

        copy($iconPath->toString(), $portletUploadPath->toString() . '/' . $iconPath->getBasename());
    }


   /**
    * (non-PHPdoc)
    * @see portlet_PortletDefinitionInterface::getConfigurationActions()
    */
    public function getConfigurationActions()
    {
        return array();
    }
}



interface portlet_PortletInterface extends Widget_Displayable_Interface
{
    /**
     * @return portlet_PortletDefinitionInterface
     */
    public function getPortletDefinition();

    /**
     * Initialize the portlet instance id.
     * This method is called by the portlet framework when the portlet is instanciated.
     *
     * @param string $id
     */
    public function setPortletId($id);


    /**
     * Initialize the portlet instance configuration.
     * This method is called by the portlet framework when the portlet is instanciated.
     *
     * @param Array $configuration
     */
    public function setPreferences(Array $configuration);

    public function setPreference($name, $value);


    /**
     * Get the title defined in preferences or a default title for the widget frame
     * Optionnal method
     * @return string
     */
    // public function getPreferenceTitle();

}
