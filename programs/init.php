<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';



function portlets_onDeleteAddon()
{
	include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';

	$addon = bab_getAddonInfosInstance('portlets');

	bab_removeEventListener('bab_eventBeforePageCreated' ,'portlets_onBeforePageCreated', $addon->getPhpPath().'init.php');
	bab_removeEventListener('bab_eventEditorFunctions' ,'portlets_onEditorFunctions', $addon->getPhpPath().'reference_event.php');
	bab_removeEventListener('bab_eventReference' ,'portlets_onReference', $addon->getPhpPath().'reference_event.php');
	return true;
}

function portlets_upgrade($sVersionBase, $sVersionIni)
{
	global $babDB;
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';

	$addonName = 'portlets';
	$addonInfo = bab_getAddonInfosInstance($addonName);

	$addonPhpPath = $addonInfo->getPhpPath();

	$functionalities = new bab_functionalities();
	$functionalities->registerClass('Func_Portlets', $addonPhpPath . 'portlets.class.php');

	$functionalities->registerClass('Func_PortletBackend', $addonPhpPath . 'portletbackend.class.php');

	$functionalities->register('Ovml/Function/PortletContainer', dirname(__FILE__).'/ovml.php');

	if (function_exists('bab_removeAddonEventListeners')) {
	    bab_removeAddonEventListeners('portlets');
	}

	bab_addEventListener('bab_eventBeforePageCreated', 'portlets_onBeforePageCreated', $addonPhpPath.'init.php', 'portlets');
	bab_addEventListener('bab_eventEditorFunctions', 'portlets_onEditorFunctions', $addonPhpPath.'reference_event.php', 'portlets');
	bab_addEventListener('bab_eventReference', 'portlets_onReference', $addonPhpPath.'reference_event.php', 'portlets');


	// create tables
	require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
	require_once dirname(__FILE__).'/portletconfiguration.class.php';
	require_once dirname(__FILE__).'/portletdefinitionconfiguration.class.php';
	require_once dirname(__FILE__).'/containerconfiguration.class.php';
	require_once dirname(__FILE__).'/category.class.php';
	require_once dirname(__FILE__).'/portletcategory.class.php';

	$mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);

	$sql = new bab_synchronizeSql();

	$queries = $mysqlbackend->setToSql(new portlets_PortletConfigurationSet());
	$queries .= $mysqlbackend->setToSql(new portlets_ContainerConfigurationSet());
	$queries .= $mysqlbackend->setToSql(new portlets_PortletDefinitionConfigurationSet());
	$queries .= $mysqlbackend->setToSql(new portlets_CategorySet());
	$queries .= $mysqlbackend->setToSql(new portlets_PortletCategorySet());
	$queries .= "
		CREATE TABLE `portlets_manager_groups` (
		  `id` int(11) unsigned NOT NULL auto_increment,
		  `id_object` int(11) unsigned NOT NULL default '0',
		  `id_group` int(11) unsigned NOT NULL default '0',
		  PRIMARY KEY  (`id`),
		  KEY `id_object` (`id_object`),
		  KEY `id_group` (`id_group`)
		);";

	$sql->fromSqlString($queries);

	if ($sql->isCreatedTable('portlets_manager_groups'))
	{
		$babDB->db_query("INSERT INTO portlets_manager_groups (id_object, id_group) VALUES ('1', '".BAB_ADMINISTRATOR_GROUP."')");
	}


	require_once dirname(__FILE__).'/functions.php';
	portlets_populateCategory();

	return true;
}


function portlets_onBeforePageCreated(bab_eventBeforePageCreated $event)
{
	$babBody = bab_getBody();
	$addon = bab_getAddonInfosInstance('portlets');

	if ($jquery = bab_functionality::get('jquery'))
	{
		$jquery->includeCore();
	}

	$babBody->addJavascriptFile($addon->getTemplatePath().'parser.js' , true);
	$babBody->addStyleSheet($addon->getStylePath().'portlets.css');
}



function portlets_getAdminSectionMenus(&$url, &$text)
{
	require_once dirname(__FILE__) . '/functions.php';
	static $nbMenus = 0;
	if (!$nbMenus) {
		$addon = bab_getAddonInfosInstance('portlets');
		$url = $addon->getUrl() . 'admin';
		$text = portlets_translate('Portlets');
		$nbMenus++;
		return true;
	}
	return false;
}


