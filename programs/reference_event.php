<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/reference.class.php';



/**
 * Called by wysiwig editor.
 *
 * Adds entries to the ovidentia button menu.
 *
 * @param bab_eventEditorFunctions $event
 */
function portlets_onEditorFunctions(bab_eventEditorFunctions $event)
{
    $version = bab_getDbVersion();

    if (null === $version || version_compare($version, '7.8.1', '<')) {
        // un bug dans le traitement des references ne permet pas l'utilisation des parametres sur la reference avant la version 7.8.1
        return;
    }

    $addon = bab_getAddonInfosInstance('portlets');

    $event->addFunction(
        portlets_translate('Portlets'),
        portlets_translate('Insert a portlet'),
        '?tg=addon/portlets/editor',
        '../' . $addon->getImagesPath() . 'portlet16.png'
    );
}




function portlets_onReference(bab_eventReference $oEvent)
{
	$oRefStorage		= $oEvent->getReferences('portlets.portlet');
	$oRefDescStorage	= $oEvent->getReferencesDescriptions('portlets.portlet');

	if($oRefStorage instanceof bab_ObjectStorage && $oRefDescStorage instanceof bab_ObjectStorage)
	{
		foreach($oRefStorage as $oReference)
		{
			$oRefDescStorage->attach(new portlet_PortletReferenceDescription($oReference));
		}
	}
}




/**
 * Portlet reference description
 * ovidentia:///portlets/portlet/backend_name/portlet_definition_id?param1=value1&param2=value2
 */
class portlet_PortletReferenceDescription extends bab_ReferenceDescriptionImpl
{
	/**
	 *
	 * @var portlet_PortletInterface
	 */
	private $portlet = null;

	public function getType()
	{
		return portlets_translate('Portlet');
	}


	/**
	 * @return portlet_PortletInterface
	 */
	private function getPortlet()
	{
		$parameters = (array) $this->getParameters();

		$parameters = bab_getStringAccordingToDataBase($parameters, bab_charset::UTF_8);

		if (null === $this->portlet)
		{
			$reference = $this->getReference();
			list($backend_name, $portlet_id) = explode('/', $reference->getObjectId());

			$backend = @bab_Functionality::get('PortletBackend/' . $backend_name);
			/*@var $backend Func_PortletBackend */

			if (false !== $backend)
			{
				$this->portlet = $backend->getPortlet($portlet_id);
			} else {
				$this->portlet = null;
			}

			//You does not have access to this portlet
			if(!isset($this->portlet)){
				return null;
			}

			if (isset($parameters['__id']))
			{
				$this->portlet->setPortletId($parameters['__id']);
			}
		}

		$this->portlet->setPreferences($parameters);


		return $this->portlet;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		$portlet = $this->getPortlet();

		if (false === $portlet)
		{
			return '';
		}

		return $portlet->getPortletDefinition()->getName();
	}

	/**
	 * @return string	HTML
	 */
	public function getDescription() {
		$W = bab_Widgets();

		$portlet = $this->getPortlet();

		if (false === $portlet)
		{
			return '';
		}

		$frame = $W->Frame()->addClass('portlet-content');
		$frame->addItem($portlet);

		return $frame->display($W->HtmlCanvas());
	}

	/**
	 * @return string
	 */
	public function getUrl() {
		return '';
	}

	/**
	 * @throws Exception if not a valid file
	 * @return bool
	 */
	public function isAccessValid()
	{
		$portlet = $this->getPortlet();

		if (false === $portlet)
		{
			return false;
		}
		return true;
	}
}