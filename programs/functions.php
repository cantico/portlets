<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 *
 * @param string $str
 * @return string
 */
function portlets_translate($str)
{
	return bab_translate($str, 'portlets');
}



// /**
//  *
//  * @param portlet_PortletDefinitionInterface $definition
//  * @return string | false
//  */
// function portlet_getPortletIcon(portlet_PortletDefinitionInterface $definition)
// {
//     return $definition->getIcon();
// }



/**
 *
 * @param string $containerId
 * @param int    $userId
 *
 * @return portlets_PortletConfiguration[]
 */
function portlets_getContainerPortletConfigurations($containerId, $userId)
{
	if (!isset($userId) && bab_isUserLogged()) {
		$userId = $GLOBALS['BAB_SESS_USERID'];
	}

	require_once dirname(__FILE__) . '/portletconfiguration.class.php';

	$portletConfigurationSet = new portlets_PortletConfigurationSet();

	$portletConfigurations = $portletConfigurationSet->select(
		$portletConfigurationSet->container->is($containerId)
		->_AND_($portletConfigurationSet->user->is($userId))
	);

	$portletConfigurations->orderAsc($portletConfigurationSet->position);

	return $portletConfigurations;
}



/**
 *
 * @param string $containerId
 *
 * @return portlets_ContainerConfiguration
 */
function portlets_getContainerConfiguration($containerId)
{
	require_once dirname(__FILE__) . '/containerconfiguration.class.php';

	$containerConfigurationSet = new portlets_ContainerConfigurationSet();

	$containerConfigurations = $containerConfigurationSet->select(
		$containerConfigurationSet->container->is($containerId)
	);


	foreach ($containerConfigurations as $containerConfiguration) {
		break;
	}

	if (!isset($containerConfiguration)) {
		$containerConfiguration = $containerConfigurationSet->newRecord();
		$containerConfiguration->container = $containerId;
		$containerConfiguration->locked = 0;
	}

	return $containerConfiguration;
}


/**
 *
 * @param string    $containerId
 * @param int       $userId
 */
function portlets_getContainerPortlets($containerId, $userId = null)
{
	require_once dirname(__FILE__) . '/frame.class.php';
	require_once dirname(__FILE__) . '/portletdefinitionconfiguration.class.php';


	if (!isset($userId) && bab_isUserLogged()) {
		$userId = bab_getUserId();
	}

	if (null === $userId) {
		return array();
	}

	$containerConfiguration = portlets_getContainerConfiguration($containerId);
	$portletConfigurations = portlets_getContainerPortletConfigurations($containerId, $userId);


	$portletDefinitionConfigurationSet = new portlets_PortletDefinitionConfigurationSet();

	$portlets = array();
	foreach ($portletConfigurations as $portletConfiguration) {
		$portletId = $portletConfiguration->portletDefinition;

		$portletBackendPath = 'PortletBackend/' . $portletConfiguration->backend;

		$portletDefinitionConfiguration = $portletDefinitionConfigurationSet->get(
			$portletDefinitionConfigurationSet->backend->is($portletConfiguration->backend)
			->_AND_($portletDefinitionConfigurationSet->portletDefinition->is($portletId))
		);

		if ($portletDefinitionConfiguration && !$portletDefinitionConfiguration->active) {
			continue;
		}

		/* @var $portletBackend Func_PortletBackend */

		$portletBackend = @bab_Functionality::get($portletBackendPath);
		if (false === $portletBackend) {
			continue;
		}

		$portlet = $portletBackend->getPortlet($portletId);

		if (null === $portlet) {
			// the definition has been deleted from the target backend
			// portlet ignored
			continue;
		}

		$portlet->setPortletId($portletConfiguration->id);

		$preferences = array();
		if ('' !== $portletConfiguration->preferences) {
		    $currentPreferences = unserialize($portletConfiguration->preferences);
		    if (is_array($currentPreferences)) {
		        foreach($currentPreferences as $k => $v){
		            $preferences[$k] = $v;
		        }
		    }
		}

		if (!empty($preferences)) {
			$portlet->setPreferences($preferences);
		}

		if (method_exists($portlet, 'getPreferenceTitle')) {
			$title = $portlet->getPreferenceTitle();
			bab_debug($title);
		} else {
			$title = null;
			if (isset($preferences['_blockTitle']) && !empty($preferences['_blockTitle'])) {
				$title = $preferences['_blockTitle'];
			}
		}

		$portletFrame = new portlets_Frame($portletConfiguration->id, $title);

		$portletFrame->setPortlet($portlet);
		$portletFrame->setRemovable($containerConfiguration->isConfigurable());
		$portletFrame->setConfigurable($containerConfiguration->isConfigurable());

		if (isset($preferences['_blockWidth']) && !empty($preferences['_blockWidth'])) {
		    $portletFrame->setSizePolicy($preferences['_blockWidth']);
		}
		if (isset($preferences['_blockHeight']) && !empty($preferences['_blockHeight'])) {
		    $result = portlet_preferenceHeightToArray($preferences['_blockHeight']);
		    
		    $height = $result['value'];
		    $unit = $result['unit'];
		    
		    $portletFrame->setCanvasOptions(Widget_Item::options()->height($height, $unit));
		}
		if (isset($preferences['_blockClassname']) && !empty($preferences['_blockClassname'])) {
		    $portletFrame->addClass($preferences['_blockClassname']);
		}

		$portlets[] = $portletFrame;
	}
	return $portlets;
}





/**
 * @param portlets
 * @param preferences
 * @param currentPreferences
 * @param preferences
 * @param preferences
 */

function portlet_getDefaultPreferences($portletConfiguration)
{
    $preferences = array();
    $portletDefinition = $portletConfiguration->getDefinition();
    $settings = $portletDefinition->getPreferenceFields();
    foreach ($settings as $setting) {
    	if (isset($setting['default'])) {
    		$preferences[$setting['name']] = $setting['default'];
    	}
    }
    return $preferences;
}

function portlet_preferenceHeightToArray($givenHeight)
{
    $regex = '/(?P<value>[0-9.]+)(?P<unit>[a-z%]*)/';
    $result = array();
    preg_match($regex, $givenHeight, $result);
    
    $height = $result['value'];
    $unit = !empty($result['unit']) ? $result['unit'] : 'px';
    
    return array(
        'value' => $height,
        'unit' => $unit
    );
}

/**
 * Checks if a user can manage the specified container (or any container).
 *
 * @param string $userId      The user id or null for current user.
 * @param string $container   The container id or null for global access. Since 0.15
 * @return boolean
 */
function portlets_userIsPortletManager($userId = null, $container = null)
{
	if (!isset($userId)) {
		$userId = $GLOBALS['BAB_SESS_USERID'];
	}
	if (!$userId) {
		return false;
	}
	if (!isset($container)) {
	    return bab_isAccessValid('portlets_manager_groups', '0', $userId);
	}

	$containerConfiguration = portlets_getContainerConfiguration($container);
    return bab_isAccessValid('portlets_manager_groups', '0', $userId) || bab_isAccessValid('portlets_manager_groups', $containerConfiguration->id, $userId);
}


/**
 * Returns a layout populated with all the portlets associated to
 * the specified container.
 *
 * @param string    $containerId
 * @param bool      $locked
 *
 * @return Widget_Layout
 */
function portlets_getContainerLayout($containerId, $locked = null)
{
	$containerConfiguration = portlets_getContainerConfiguration($containerId);


	if (null === $locked)
	{
		$locked = (bool) $containerConfiguration->locked;
	} else {
		// insert configuration if not exists
		$containerConfiguration->locked = $locked;
	}

	if (empty($containerConfiguration->id))
	{
		$containerConfiguration->save();
	}


	if ($locked) {
		$userId = 0;
	} else {
		$userId = null;
	}

	$W = bab_Widgets();
	$containerLayout = $W->Layout();

	$portlets = portlets_getContainerPortlets($containerId, $userId);
	foreach ($portlets as $portletFrame) {

	    $portletFrame->setSizePolicy($portletFrame->getSizePolicy() . ' portlet-container-item');
		$containerLayout->addItem(
			$portletFrame
		);

	}

	return $containerLayout;
}


/**
 * Returns html populated with all the portlets associated to
 * the specified container.
 *
 * @param string $containerId
 *
 * @return string
 */
function portlets_getContainerHtml($containerId)
{
	$containerConfiguration = portlets_getContainerConfiguration($containerId);

	$W = bab_Widgets();
	$canvas = $W->HtmlCanvas();

	$containerLayout = portlets_getContainerLayout($containerId);

	$containerContent = $containerLayout->display($canvas);

	$classname = '';

	if ($containerConfiguration->locked) {
		$classname .= ' portlets-locked';
	}
	if ($containerConfiguration->isLockable()) {
		$classname .= ' portlets-lockable';
	}
	if ($containerConfiguration->isSortable()) {
		$classname .= ' portlets-sortable';
	}
	if ($containerConfiguration->isConfigurable()) {
		$classname .= ' portlets-configurable';
	}

	$output = '<div class="bab-portlet-container bab-portlets-container-' . $containerId . ' ' . $classname . '">'
	. $containerContent
	. '</div>';

	return $output;
}


/**
 * Populate category table with category which comes from
 * different backends
 *
 * return bool $new true if a new category has been added false otherwise.
 */
function portlets_populateCategory()
{
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	require_once dirname(__FILE__).'/portletdefinitionconfiguration.class.php';
	require_once dirname(__FILE__).'/category.class.php';
	require_once dirname(__FILE__).'/portletcategory.class.php';

	$func = new bab_Functionalities();

	$backendNames = $func->getChildren('PortletBackend');

	bab_functionality::includefile('PortletBackend');

	$categorySet = new portlets_CategorySet();
	$portletCategorySet = new portlets_PortletCategorySet();

	$new = false;
	foreach ($backendNames as $backendName) {
		$backendPath = trim('PortletBackend/' . $backendName, '/');

		/* @var $backend Func_PortletBackend */
		$backend = bab_Functionality::get($backendPath);

		if (false === $backend) {
			continue;
		}

		$categories = $backend->getCategories();

		foreach ($categories as $categoryName) {
		    $category = $categorySet->get($categorySet->name->is($categoryName));
			if (!$category) {
				$category = $categorySet->newRecord();
				$category->name = $categoryName;
				$category->from = $backendName;
				$category->disabled = false;
				$category->save();
				$new = true;
			}

			if (method_exists($backend, 'select')) {
				$portletDefinitions = $backend->select($categoryName);

				foreach ($portletDefinitions as $definition) {
				    $portletCategory = $portletCategorySet->get($portletCategorySet->portlet->is($definition->getId())->_AND_($portletCategorySet->category->is($category->id)));
					if (!$portletCategory) {
						$portletCategory = $portletCategorySet->newRecord();
						$portletCategory->portlet = $definition->getId();
						$portletCategory->category = $category->id;
						$portletCategory->save();
					}
				}
			}
		}
	}

	return $new;
}
